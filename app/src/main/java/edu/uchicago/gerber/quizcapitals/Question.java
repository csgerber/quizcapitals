package edu.uchicago.gerber.quizcapitals;

import java.util.HashSet;
import java.util.Set;

public class Question {

    private String country;
    private String capital;
    private String region;

    public Question(String country, String capital, String region) {
        this.country = country;
        this.capital = capital;
        this.region = region;
    }

    private Set<String> wrongAnswers = new HashSet<>();

    public String getCountry() {
        return country;
    }

    public String getCapital() {
        return capital;
    }

    public String getRegion() {
        return region;
    }

    public Set<String> getWrongAnswers() {
        return wrongAnswers;
    }

    public void addWrongAnswer(String wrong){
        wrongAnswers.add(wrong);
    }

    public String getQuestionText(){
        return "What is the capital of " + country + "?";
    }



}
